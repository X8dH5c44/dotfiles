require 'autocommands'

require 'config.plugins'
require 'config.options'
require 'config.keymap'
require 'config.highlights'

require 'plugins.coc'
require 'plugins.vimtex'

