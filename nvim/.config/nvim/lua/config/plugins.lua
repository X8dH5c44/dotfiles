return require('packer').startup(
function(use)
  use { 'wbthomason/packer.nvim' }
  use { 'neoclide/coc.nvim', branch='release' }
  use { 'alx741/vim-rustfmt' }
  use { 'williamboman/mason.nvim', run = ':MasonUpdate' }
  use { 'ms-jpq/chadtree', branch='chad' }
  use { 'tpope/vim-commentary' }
  use { 'lervag/vimtex', branch='master' }
end
)

