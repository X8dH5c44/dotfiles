vim.opt.expandtab = true
vim.opt.tabstop = 2
vim.opt.shiftwidth = 2
vim.opt.shiftround = true
vim.opt.hidden = true
vim.opt.number = true
vim.opt.cursorline = true
vim.opt.title = true
vim.opt.wildmode = { 'longest:full', 'full' }
vim.opt.wrap = false
vim.opt.list = true
vim.opt.listchars = { tab = '▶ ', trail = '·' }
vim.opt.scrolloff = 3
vim.opt.confirm = true
vim.opt.foldmethod = 'syntax'
vim.opt.foldnestmax = 1

vim.cmd([[
  filetype plugin indent on
  syntax on
]])

vim.api.nvim_create_autocmd({ 'VimEnter' }, { command = 'CHADopen' })

