vim.cmd([[
  hi Conceal ctermbg=none
  hi LineNr ctermfg=Yellow
  hi CursorLineNr cterm=bold ctermfg=Yellow ctermbg=black
  hi CursorLine cterm=bold ctermbg=black
  hi ColorColumn ctermbg=red
]])

