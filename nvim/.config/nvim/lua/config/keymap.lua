vim.g.mapleader = ' '
vim.keymap.set('n', '<leader><space>', 'za')
vim.keymap.set({}, 'gf', 'edit <cfile><cr>')
vim.keymap.set('n', 'Y', 'y$')

