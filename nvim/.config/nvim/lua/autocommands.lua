local autocmd = vim.api.nvim_create_autocmd

autocmd('BufWritePost', {
  desc = 'Automatically source init.lua on write',
  command = 'source ~/.config/nvim/init.lua',
  pattern = 'init.lua'
})
