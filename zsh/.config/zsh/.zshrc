# don't load if not interactive
[[ $- != *i* ]] && return

# enable colors
autoload -U colors && colors

# print available colors
# for key val in "${(@kv)bg}"
# do
#   echo "$val$key"
# done

setopt PROMPT_SUBST
source ~/.config/zsh/plugins/git-prompt.sh
spc=''

color_1="yellow"
color_2="white"
color_3="grey"
def_fg="%{$fg[white]%}"
def_bg="%{$bg[default]%}"
prompt_fg="%{$fg[black]%}"
fst_fg="%{$fg[$color_1]%}"
scd_fg="%{$fg[$color_2]%}"
thr_fg="%{$fg[$color_3]%}"
fst_bg="%{$bg[$color_1]%}"
scd_bg="%{$bg[$color_2]%}"
thr_bg="%{$bg[$color_3]%}"

# differentiate between tty and pts 
if [[ $(tty) = *tty* ]]; then
  # tty
  setleds -D +num
  GIT_PS1_SHOWCOLORHINTS=0
  spc='>'
  sec1="$fst_fg$def_bg"
  sec2="$scd_fg$def_bg"
  sec3="$thr_fg$def_bg"
  sec4="$def_fg$def_bg"
  sepc1="$def_fg$def_bg"
  sepc2="$def_fg$def_bg"
  sepc3="$def_fg$def_bg"
else
  # pts
  GIT_PS1_SHOWCOLORHINTS=1
  spc=$'\ue0b0'
  sec1="$prompt_fg$fst_bg"
  sec2="$prompt_fg$scd_bg"
  sec3="$prompt_fg$thr_bg"
  sec4="$def_fg$def_bg"
  sepc1="$fst_fg$scd_bg"
  sepc2="$scd_fg$thr_bg"
  sepc3="$thr_fg$def_bg"
fi

GIT_PS1_SHOWDIRSTATE=1
GIT_PS1_SHOWSTASHSTATE=1
GIT_PS1_SHOWUNTRACKEDFILES=1
GIT_PS1_DESCRIBE_STYLE="branch"
GIT_PS1_SHOWUPSTREAM="auto git"

RPS1="%B%(?.%F{green}.%F{red})%?%f%b"
sep1="$sepc1$spc"
sep2="$sepc2$spc"
sep3="$sepc3$spc"
precmd() {  __git_ps1 "%B$sec1%n@%M $sep1$sec2 %~ $sep2$sec3 " "$sep3$sec4 %b" "%s " }

# history
HISTFILE=~/.cache/zsh/history
HISTSIZE=1000
SAVEHIST=1000

# basic autocomplete
autoload -Uz compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
# add dotfiles to autocomplete
_comp_options+=(globdots)

# case insensitive autocomplete
#zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}'
#zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=*' 'l:|=* r:|=*'

# vim mode
bindkey -v
export KEYTIMEOUT=1
# bindkey "^[^[C" vi-forward-word
# bindkey "^[^[D" vi-backward-word

if [ "$TERM" = "linux" ]; then
  echo -en "\e]P0151718" # black        -> grey
  echo -en "\e]P0000000" # black        -> black
  echo -en "\e]P1cd3f45" # dark red     -> red
  echo -en "\e]P29fca56" # dark green   -> green
  echo -en "\e]P3e6cd69" # brown        -> yellow
  echo -en "\e]P455b5db" # dark blue    -> blue
  echo -en "\e]P5a074c4" # dark magenta -> purple
  echo -en "\e]P655dbbe" # dark cyan    -> blue
  echo -en "\e]P7d6d6d6" # light grey   -> light grey
  echo -en "\e]P8151718" # dark grey    -> grey
  echo -en "\e]P9cd3f45" # red          -> red
  echo -en "\e]PA9fca56" # green        -> green
  echo -en "\e]PBe6cd69" # yellow       -> yellow
  echo -en "\e]PC55b5db" # blue         -> blue
  echo -en "\e]PDa074c4" # magenta      -> purple
  echo -en "\e]PE55dbbe" # cyan         -> blue
  echo -en "\e]PFd6d6d6" # white        -> light grey
  clear
fi

setopt autocd

#unsetopt beep extendedglob nomatch notify
#bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
#zstyle :compinstall filename '/home/kolja/.zshrc'

export EDITOR=nvim
export VISUAL=nvim
export BROWSER=firefox

setdisplay() {
  case ${1} in
    "hdmi") xrandr --output HDMI1 --auto --primary --output eDP1        --off;;
    "both") xrandr --output HDMI1 --auto --primary --output eDP1 --auto --right-of HDMI1;;
    "")     xrandr --output HDMI1        --off     --output eDP1 --auto --primary ;;
  esac
}
setwp() {
  if [ $# -gt 3 ]; then
    echo 'Too many arguments! [>2]'
    return
  fi
  lock=false
  desktop=false
  wallpaper=''
  for item in "$@"
  do  
    case "$item" in
      '--lock'|'-l')
        lock=true;;
      '--both'|'-a')
        lock=true
        desktop=true;;
      *) wallpaper=$item;;
    esac
  done
  if [ "$lock" = false ]; then
    desktop=true
  fi
  if [ "$lock" = true ]; then
    sudo cp $wallpaper /usr/share/lightdm-gtk-greeter-settings/wallpaper
  fi
  if [ "$desktop" = true ]; then
    cp $wallpaper ~/.wallpaper
    feh --bg-fill ~/.wallpaper
  fi
}
bat-info() {
  full_info="$(upower -i `upower -e | grep 'BAT'`)"
  percentage="$(echo $full_info | grep percentage)"
  time_to_empty="$(echo $full_info | grep 'time to empty')"
  echo $percentage
  echo $time_to_empty
}
vol () {
  case $1 in
    "up")     pactl set-sink-volume @DEFAULT_SINK@ +5% ;;
    "down")   pactl set-sink-volume @DEFAULT_SINK@ -5% ;;
    "toggle") pactl set-sink-mute @DEFAULT_SINK@ toggle ;;
    "mute")   pactl set-sink-mute @DEFAULT_SINK@ 1 ;;
    "unmute") pactl set-sink-mute @DEFAULT_SINK@ 0 ;;
  esac
}
alias ls='ls -1 --color=auto'
alias dir='dir --color=auto'
alias vdir='vdir --color=auto'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

alias stowconf='stow -t ~ -d ~/git/dotfiles/'
alias mkdir="mkdir -p"
alias sh-cheatsheet="firefox https://steinbaugh.com/posts/posix.html"
alias l='ls -lh'
alias ll='ls -lha'
alias ytdl="~/.bin/ytdl"
alias pympress="~/.local/bin/pympress"
alias zzz='sudo systemctl suspend && slock & exit'
alias ZZZ='sudo systemctl hibernate && slock & exit'
alias vim=nvim
alias doastest='sudo doas -C /etc/doas.conf && echo "config ok" || echo "config error"'

export PATH=$PATH:~/.local/bin

if [[ $(tty) == '/dev/tty1' ]]; then
  startx
fi

