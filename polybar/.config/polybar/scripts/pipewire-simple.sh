#!/bin/sh

getDefaultSink() {
  defaultSink=$(pactl info | awk -F : '/Default Sink:/{print $2}')
  description=$(pactl list sinks | sed -n "/${defaultSink}/,/Description/s/^\s*Description: \(.*\)/\1/p")
  echo "${description}"
}

getDefaultSource() {
  defaultSource=$(pactl info | awk -F : '/Default Source:/{print $2}')
  description=$(pactl list sources | sed -n "/${defaultSource}/,/Description/s/^\s*Description: \(.*\)/\1/p")
  echo "${description}"
}

MUTED=$(pamixer --get-volume-human)
VOLUME=$(pamixer --get-volume)
SINK=$(getDefaultSink)
SOURCE=$(getDefaultSource)

case $1 in
  "--up")
    pamixer --increase 5
    ;;
  "--down")
    pamixer --decrease 5
    ;;
  "--mute")
    pamixer --toggle-mute
    ;;
  *)
    if [ $MUTED != 'muted' ]; then
      echo " $VOLUME%"
    else
      echo " $VOLUME%"
    fi
#   echo "Source: ${SOURCE} | Sink: ${VOLUME} ${SINK}"
esac
