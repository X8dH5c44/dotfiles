#!/usr/bin/env sh

killall -q polybar

while pgrep -u $UID -x polybar > /dev/null; do sleep 1; done

count=$(xrandr --query | grep " connected" | cut -d" " -f1 | wc -l)

if [ $count = 1 ]; then
  m=$(xrandr --query | grep " connected" | cut -d" " -f1)
  MONITOR=$m polybar --reload mainbar-xmonad -c ~/.config/polybar/config.ini &
else
  for m in $(xrandr --query | grep " connected" | cut -d" " -f1); do
    MONITOR=$m polybar --reload mainbar-xmonad -c ~/.config/polybar/config.ini &
  done
fi
# second polybar at bottom
# if [ $count = 1 ]; then
#   m=$(xrandr --query | grep " connected" | cut -d" " -f1)
#   MONITOR=$m polybar --reload mainbar-xmonad-extra -c ~/.config/polybar/config &
# else
#   for m in $(xrandr --query | grep " connected" | cut -d" " -f1); do
#     MONITOR=$m polybar --reload mainbar-xmonad-extra -c ~/.config/polybar/config &
#   done
# fi
