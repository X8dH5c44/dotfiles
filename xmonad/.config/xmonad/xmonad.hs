import XMonad

import XMonad.Actions.Navigation2D

import XMonad.Hooks.ManageDocks
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.SetWMName
import XMonad.Hooks.WindowSwallowing

import XMonad.Layout.BinarySpacePartition
import XMonad.Layout.BorderResize
import XMonad.Layout.Gaps
import XMonad.Layout.NoBorders
import XMonad.Layout.Spacing
import XMonad.Layout.SubLayouts

import XMonad.Util.Run
import XMonad.Util.Dzen

import Graphics.X11.ExtraTypes.XF86
import System.Exit

import qualified Data.Map as M
import qualified DBus as D
import qualified DBus.Client as D
import qualified Codec.Binary.UTF8.String as UTF8
import qualified XMonad.StackSet as W

main = do
  dbus <- D.connectSession
  D.requestName dbus (D.busName_ "org.xmonad.Log")
    [D.nameAllowReplacement, D.nameReplaceExisting, D.nameDoNotQueue]
  xmonad . withNavigation2DConfig  def {defaultTiledNavigation = sideNavigation} $ ewmh $ docks settings
    { logHook = dynamicLogWithPP (myLogHook dbus)
    , startupHook = startupHook settings >> setWMName "LG3D"
    }

alert = dzenConfig return "test"

layouts = smartBorders ((avoidStruts $ borderResize emptyBSP) ||| Full)
--layouts = smartBorders ((avoidStruts $ gaps [(U,10),(R,10),(L,10),(D,10)] $ spacing 10 $ (borderResize (emptyBSP))) ||| Full)

keybinds conf@(XConfig {XMonad.modMask = modm}) = M.fromList $
  [ ((0, xF86XK_AudioRaiseVolume          ),  spawn "pactl set-sink-volume\
                                          \   @DEFAULT_SINK@ +5% &&\
                                          \   ~/.xmonad/showVolume.sh up"     ),
    ((0, xF86XK_AudioLowerVolume          ),  spawn "pactl set-sink-volume\
                                          \   @DEFAULT_SINK@ -5% &&\
                                          \   ~/.xmonad/showVolume.sh down"   ),
    ((0, xF86XK_AudioMute                 ),  spawn "pactl set-sink-mute\
                                          \   @DEFAULT_SINK@ toggle &&\
                                          \   ~/.xmonad/showVolume.sh mute"   ),
    ((0, xF86XK_MonBrightnessUp           ),  spawn "xbacklight -inc 10"      ),
    ((0, xF86XK_MonBrightnessDown         ),  spawn "xbacklight -dec 10"      ),
    ((0, xF86XK_AudioPlay                 ),  spawn "xwm-mediascript"         ),
    ((0, xF86XK_AudioPause                ),  spawn "xwm-mediascript"         ),
    ((0, xK_Print                         ),  spawn "flameshot gui"           ),
    ((modm, xK_Return                     ),  spawn "rofi -show drun"         ),
    ((modm, xK_p                          ),  spawn "rofi -show run"          ),
    ((modm, xK_m                          ),  spawn "kitty cxxmatrix\
                                          \   --no-diffuse\
                                          \   --scene rain-forever\
                                          \   --preserve-background\
                                          \   --color yellow"                 ),
    ((modm, xK_t                          ),  withFocused $ windows . W.sink  ),
    ((modm, xK_comma                      ),  sendMessage(IncMasterN 1)       ),
    ((modm, xK_period                     ),  sendMessage(IncMasterN (-1))    ),
    ((modm, xK_q                          ),  spawn "xmonad --recompile;\
                                          \   xmonad --restart"               ),
    ((modm, xK_Right                      ),  windowGo R False                ),
    ((modm, xK_Left                       ),  windowGo L False                ),
    ((modm, xK_Up                         ),  windowGo U False                ),
    ((modm, xK_Down                       ),  windowGo D False                ),
    ((modm, xK_space                      ),  sendMessage NextLayout          ),
    ((modm .|. shiftMask, xK_space        ),  spawn "polybar-msg cmd toggle"  ),
    ((modm .|. shiftMask, xK_Right        ),  windowSwap R False              ),
    ((modm .|. shiftMask, xK_Left         ),  windowSwap L False              ),
    ((modm .|. shiftMask, xK_Up           ),  windowSwap U False              ),
    ((modm .|. shiftMask, xK_Down         ),  windowSwap D False              ),
    ((modm .|. shiftMask, xK_q            ),  io(exitWith ExitSuccess)        ),
    ((modm .|. shiftMask, xK_Return       ),  spawn $ XMonad.terminal conf    ),
    ((modm .|. shiftMask, xK_c            ),  kill                            )]
    ++
    [((m .|. modm, k), windows $ f i)
        | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_9]
        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]
    ++
    [((m .|. modm, key), screenWorkspace sc >>= flip whenJust (windows . f))
        | (key, sc) <- zip [xK_w, xK_e, xK_r] [0..]
        , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]

myLogHook :: D.Client -> PP
myLogHook dbus = def
    { ppOutput = dbusOutput dbus
    , ppCurrent = wrap  ("%{F#0000ff} ") " %{F-}"
    , ppVisible = wrap  ("%{F#777777} ") " %{F-}"
    , ppUrgent = wrap   ("%{F#ff0000} ") " %{F-}"
    , ppHidden = wrap   ("%{F#777777} ") " %{F-}"
    , ppTitle = wrap    ("%{F#eeeeee} ") " %{F-}"
    }
dbusOutput :: D.Client -> String -> IO ()
dbusOutput dbus str = do
    let signal = (D.signal objectPath interfaceName memberName) {
            D.signalBody = [D.toVariant $ UTF8.decodeString str]
        }
    D.emit dbus signal
  where
    objectPath = D.objectPath_ "/org/xmonad/Log"
    interfaceName = D.interfaceName_ "org.xmonad.Log"
    memberName = D.memberName_ "Update"


startUp = do
  spawn "~/.config/xmonad/onInit.sh"

settings = def {
  terminal            = "kitty"
  , modMask             = mod4Mask
  , layoutHook          = layouts
  , keys                = keybinds
  , borderWidth         = 0
  , normalBorderColor   = "#000000"
  , focusedBorderColor  = "#ebdbb2"
  , startupHook         = startUp
  , handleEventHook = swallowEventHook (className =? "kitty") (return True)
}

