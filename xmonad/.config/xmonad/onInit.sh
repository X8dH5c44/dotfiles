#!/bin/sh
setxkbmap de
setxkbmap -option caps:escape
# touchpad tapping
xinput set-prop 12 314 1
# natural scrolling
xinput set-prop 12 322 1
logpath=~/.config/xmonad/init.log

rm $logpath

log() {
  echo $1 >> $logpath
}

running() {
  return $(ps -ef | grep $1 | wc -l)
}

run() {
  running $1
  is_running=$?
  if [ $is_running -gt 1 ]; then
    log "$1 running"
    log "$1 not launched"
  else
    log "$1 not running"
    case $1 in
      'picom')
        picom --experimental-backends &
        ;;
      'redshift')
        redshift -l 51:7 &
        ;;
      'polybar')
        ~/.config/polybar/launch.sh
        ;;
      'conky')
        conky -c ~/.config/conky/Fornax-Tengwar.conf &
        ;;
      'flameshot')
        flameshot &
        ;;
      'birdtray')
        birdtray &
        ;;
      'udiskie')
        udiskie -At &
        ;;
      *)
        echo program \"$1\" not yet configured
    esac
    log "$1 launched"
  fi
  log
}

run picom
run redshift
run polybar
run conky
run flameshot
run birdtray
run udiskie

feh --bg-fill --no-fehbg ~/.wallpaper

